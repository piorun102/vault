package client

import (
	"github.com/hashicorp/vault/api"
)

type Client interface {
	GetSecret(secretPath string) (map[string]any, error)
	GetRoleName() string
}

type clientConnection struct {
	client   *api.Client
	roleName string
}

func newClientConnection(client *api.Client, roleName string) Client {
	return &clientConnection{client: client, roleName: roleName}
}
