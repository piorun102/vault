package client

import (
	"context"
	"fmt"
	"github.com/hashicorp/vault/api"
	auth "github.com/hashicorp/vault/api/auth/approle"
	"log"
	"strings"
)

func NewClientConnection(addr, roleID, secretID string) (Client, error) {
	client, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		log.Printf("unable to create client: %v\n", err)
	}

	_ = client.SetAddress(addr)

	if roleID == "" {
		return nil, fmt.Errorf("no role ID was provided in APPROLE_ROLE_ID env var")
	}

	secretId := &auth.SecretID{FromString: secretID}

	appRoleAuth, err := auth.NewAppRoleAuth(
		roleID,
		secretId,
		//auth.WithWrappingToken(), // Only required if the secret ID is response-wrapped.
	)

	if err != nil {
		return nil, fmt.Errorf("unable to initialize AppRole auth method: %w", err)
	}

	authInfo, err := client.Auth().Login(context.Background(), appRoleAuth)

	if err != nil {
		return nil, fmt.Errorf("unable to login to AppRole auth method: %w", err)
	}
	if authInfo == nil {
		return nil, fmt.Errorf("no auth info was returned after login: %w", err)
	}

	return newClientConnection(client, authInfo.Auth.Metadata["role_name"]), nil
}

func (approle *clientConnection) GetSecret(secretPath string) (map[string]any, error) {
	secret, err := approle.client.KVv2("secret").Get(context.Background(), secretPath)

	if err != nil {
		if strings.Contains(err.Error(), "permission denied") {
			return nil, fmt.Errorf("permission denied")
		}
		if strings.Contains(err.Error(), "secret not found") {
			return nil, fmt.Errorf("secret not found")
		}
		return nil, err
	}

	if secret == nil || len(secret.Data) == 0 {
		return nil, fmt.Errorf("empty secret")
	}

	return secret.Data, nil
}

func (approle *clientConnection) GetRoleName() string {
	return approle.roleName
}
