# Vault

## Разбор конфигурации клиента


```go
package main

import (
    "gitlab.com/bongerka/vault"
    "project/config"
)

func main() {
    var cfg config.Config
    vault.GetConfig(&cfg)
}
```


## Использование

### AppRole client:

Получение секретов:
```go
package main

import (
    "fmt"
    "gitlab.com/bongerka/vault/client"
    "log"
)

func main() {
    roleID := ""
    secretID := ""
    addr := "http://..."
    
    c, err := client.NewClientConnection(addr, roleID, secretID)
    
    if err != nil {
        panic(err)
    }
    
    secret, err := c.GetSecret("secretPath")
    if err != nil {
        panic(err)
    }
    
    fmt.Println(secret)
}
```

### Root:

Создание юзера AppRole:

```go
package main

import (
    "fmt"
    "github.com/bongerka/vault/client"
    "github.com/bongerka/vault/root"
    "log"
)

func main() {
    rootToken := ""
    addr := ""
    roleName := ""
    policies := []string{""}
    
    r := root.NewRootConnection(addr, rootToken)
    
    r.CreateNewRole(roleName, policies)
    roleID := r.ReadRoleID(roleName)
    secretID := r.ReadSecretID(roleName)
    
    _, err := client.NewClientConnection(addr, roleID, secretID)
    if err != nil {
        log.Fatalln("Unable to create user", err)
        return
    }
    
    fmt.Println(fmt.Sprintf("User created with:\nrole-id: %s\nsecret-id: %s", roleID, secretID))
}