package internal

type (
	YmlConfig struct {
		Vault vaultConfig `mapstructure:"vault"`
	}

	vaultConfig struct {
		Addr     string `mapstructure:"addr"`
		RoleId   string `mapstructure:"roleId"`
		SecretId string `mapstructure:"secretId"`
	}
)
