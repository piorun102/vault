package parsing

import (
	"fmt"
	"github.com/rogpeppe/go-internal/modfile"
	"github.com/spf13/viper"
	"gitlab.com/piorun102/vault/client"
	"gitlab.com/piorun102/vault/internal"
	"log"
	"os"
	"reflect"
	"sync"
)

type Parser struct {
	client   client.Client
	vaultCfg *internal.YmlConfig
	Secrets  map[string]interface{}
}

var parser *Parser
var once sync.Once

func GetParser(vaultCfg *internal.YmlConfig) *Parser {
	once.Do(func() {
		clientConn, err := client.NewClientConnection(vaultCfg.Vault.Addr, vaultCfg.Vault.RoleId, vaultCfg.Vault.SecretId)
		if err != nil {
			log.Fatal("Unable to connect client to Vault: ", err)
		}

		parser = &Parser{clientConn, vaultCfg, make(map[string]interface{})}
	})

	return parser
}

func (parser *Parser) GetMapConfig(config any) map[string]any {
	mapConfig := make(map[string]any)
	parser.getVaultPathsRecursively(config, "", mapConfig)
	return mapConfig
}

func (parser *Parser) getVaultPathsRecursively(config any, prefix string, secretPaths map[string]any) {
	value := reflect.ValueOf(config).Elem()

	if value.Kind() == reflect.Struct {
		typ := value.Type()
		for i := 0; i < typ.NumField(); i++ {
			field := typ.Field(i)
			tag := field.Tag.Get("json")
			if tag == "" {
				continue
			}
			curPath := tag

			switch tag {
			case "Users":
				curPath = "Users"
			case "Services":
				name := getServiceName()
				curPath = fmt.Sprintf("Services/%s", name)
			case "users":
				curPath = "users"
			case "services":
				name := getServiceName()
				curPath = fmt.Sprintf("services/%s", name)
			}

			path := prefix + curPath
			fieldValue := value.Field(i)
			if fieldValue.Kind() == reflect.Struct && fieldValue.Field(0).Kind() == reflect.Struct {
				subSecrets := make(map[string]any)
				parser.getVaultPathsRecursively(fieldValue.Addr().Interface(), path+"/", subSecrets)
				secretPaths[tag] = subSecrets
			} else {
				val, err := parser.client.GetSecret(path)
				if err != nil {
					secretPaths[tag] = nil
					parser.Secrets[path] = err.Error()
					continue
				}

				parser.Secrets[path] = fmt.Sprintf("%+v", val)
				secretPaths[tag] = val
			}
		}
	}
}

func getServiceName() string {
	goModPath := "go.mod"

	content, err := os.ReadFile(goModPath)
	if err != nil {
		log.Fatalln("Unable to read go.mod: ", err)
	}

	modFile, err := modfile.Parse("go.mod", content, nil)
	if err != nil {
		log.Fatalln("Unable to parse go.mod: ", err)
	}

	return modFile.Module.Mod.Path
}

func ParseApproleConfig(configPath string) *internal.YmlConfig {
	var cfg internal.YmlConfig
	viper.SetConfigFile(configPath)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalln("Unable to read Vault config: ", err)
	}

	if err := viper.Unmarshal(&cfg); err != nil {
		log.Fatalln("Unable to unmarshal Vault config: ", err)
	}

	return &cfg
}
