package vault

import (
	"encoding/json"
	"gitlab.com/piorun102/vault/internal/parsing"
	"log"
)

func GetConfig(cfg any, path ...string) {
	configPath := "config/config.yml"
	if len(path) > 0 {
		configPath = path[0]
	}
	vaultCfg := parsing.ParseApproleConfig(configPath)
	p := parsing.GetParser(vaultCfg)
	mapConfig := p.GetMapConfig(cfg)

	data, err := json.Marshal(mapConfig)
	if err != nil {
		log.Fatalln("Unable to unmarshal config data: ", err)
	}

	err = json.Unmarshal(data, &cfg)
	if err != nil {
		log.Fatalln("Unable to unmarshal config data: ", err)
	}
}

func LookAtSecrets(cfg any, path ...string) map[string]interface{} {
	configPath := "config/config.yml"
	if len(path) > 0 {
		configPath = path[0]
	}
	vaultCfg := parsing.ParseApproleConfig(configPath)
	p := parsing.GetParser(vaultCfg)
	if len(p.Secrets) == 0 {
		GetConfig(cfg)
	}

	return p.Secrets
}
